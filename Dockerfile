FROM python:3.8.6-alpine
WORKDIR .
COPY ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
COPY . /app
RUN mkdir /data
EXPOSE 5000
ENTRYPOINT [ "python" ]
CMD ["app.py"]

