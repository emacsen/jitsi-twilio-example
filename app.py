#!/usr/bin/env python

from os import environ
from flask import Flask, jsonify, request
from flask_cors import CORS,cross_origin
from tinydb import TinyDB, Query
from secrets import randbelow
from twilio.twiml.voice_response import VoiceResponse, Gather, Dial, Redirect, Sip
from json import loads

SIP_URI = environ['SIP_URI']
DB_FILE = environ.get('DB') or '/data/db.json'
PIN_DIGITS = environ.get('PIN_DIGITS') or 6
PHONE_NUMBERS = loads(environ["PHONE_NUMBERS"])

db = TinyDB(DB_FILE)

app = Flask(__name__)
cors = CORS(app)

@app.route('/callOut')
def call_out():
    "Make an outgoing call"
    to = request.args['To']
    to_formatted = to.split('@')[0].split(':')[1]
    caller_id = request.args['callerId']
    resp = VoiceResponse()
    resp.dial(to_formatted, caller_id=caller_id, answerOnBridge=True)
    return str(resp)

@app.route("/answer", methods=["GET", "POST"])
def answer():
    """Respond to incoming phone calls with a say and then a request for
    the conference pin"""
    if request.method == "GET":
        # Start our TwiML response
        resp = VoiceResponse()
        gather = Gather(num_digits=PIN_DIGITS)
        gather.say("Welcome to the conferencing system! Please enter your pin")
        resp.append(gather)
        # If no response, loop say goodbye
        resp.say("We didn't receive any input. Goodbye!")
        return str(resp)
    else:
        resp = VoiceResponse()
        pin = request.form.get("Digits")
        if not pin:
            resp.rediect("/answer")
        pin = int(pin)
        # Look up the conference name. They're stored as
        # conferencename@instance. We're going to toss the instance
        # part.
        result = db.search(Query().id == pin)
        if not result:
            resp.say("No conference associated with this pin. Goodbye")
            return str(resp)
        else:
            conference = result[0]["conference"]
            dial = Dial()
            dial.sip(f"sip:{SIP_URI}?X-Room-Name={conference}")
            resp.append(dial)
            return str(resp)


@app.route("/dialInNumbers")
def dial_in_numbers():
    """Return our available phone numbers"""
    return jsonify({
        "message": "Phone numbers available.",
        "numbers": PHONE_NUMBERS,
        "numbersEnabled": True})


@app.route("/conferenceMapper")
def conference_mapper():
    if not request.args.get("id") and not request.args.get("conference"):
        return jsonify({
            "message": "No conference or id provided",
            "conference": False,
            "id": False})
    if request.args.get("id"):
        # Map a pin to a conference
        pin = int(request.args.get("id"))
        result = db.search(Query().id == pin)
        if result:
            conference = result[0]["conference"]
            return jsonify({
                "message": "Successfully retrieved conference mapping",
                "id": pin,
                "conference": conference})
        else:
            return jsonify({
                "message": "No conference mapping was found",
                "id": pin,
                "conference": False})
        
    elif request.args.get("conference"):
        # Either return the pin for the conference or else create one
        conference = request.args.get("conference")
        result = db.search(Query().conference == conference)
        if result:
            pin = result[0]["id"]
            return jsonify({
                "message": "Successfully retrieved conference mapping",
                "id": pin,
                "conference": conference})
        else:
            max_int = pow(10, PIN_DIGITS)
            while True:
                pin = randbelow(max_int)
                result = db.search(Query().id == pin)
                if not result:
                    # Create the new record
                    db.insert({"id": pin, "conference": conference})
                    return jsonify({
                        "message": "Successfully retrieved conference mapping",
                        "id": pin,
                        "conference": conference})

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
